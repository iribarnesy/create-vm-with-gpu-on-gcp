# Configurer une VM pour utiliser une GPU

## Création de la VM

* Compute engine => instance VM => creer
* Région : Europe-west1 (Belgique)
* Zone : europe-west1-b
* Config de machine => Série : N1
* Type de machine : n1-standard-8 (30go)

* Ajouter un GPU : NVIDIA Tesla K80
* Disque de démarrage : Ubuntu 16.04 LTS (A VOIR, UBUNTU 20
* Cocher Autoriser le trafic HTTP et HTTPS

## Configuration des firewalls pour Jupyter

Sur le site de Google Cloud platform, allez dans [Réseaux VPC, ](https://console.cloud.google.com/networking/networks/)

### Créer une IP externe

[addresse IP Externe](https://console.cloud.google.com/networking/addresses/) :

* Passez de éphémère à static
* Mettre un nom sur l'addresse IP

### Ajouter une règle firewall

Dans [pare-feu](https://console.cloud.google.com/networking/firewalls) 

* Vérifiez default-allow-ssh => tcp:22
* Dans cible choissisez "toutes les instances du réseau"
* Dans plage d'adresse IP : 0.0.0.0/0
* Protocole et port spécifié: TCP 22: 8889

## Ajout des packets/drivers sur la VM

Puis connectez-vous en SSH à votre VM, et suivez les commandes :

* `sudo apt-get update`
* `sudo add-apt-repository ppa:graphics-drivers/ppa`
* `sudo apt install nvidia-410`

## Environnement anaconda, Jupyter, etc 

### Installez Anaconda3

* `wget http://repo.continuum.io/archive/Anaconda3-5.1.0-Linux-x86_64.sh`
* `bash Anaconda3-5.1.0-Linux-x86_64.sh`
* `source ~/.bashrc`

Vérifier que l'environnement est ok :

* `conda activate`
* `jupyter notebook --ip=0.0.0.0 --port=8889 --no-browser &`

Accéder au Jupyter :

* copier l'adresse à partir de "?token=....."
* Ajouter devant le "?" l'adresse externe de votre VM (dans instance VM) et :8889.n

L'url que vous entrez dans le navigateur doit avoir la forme : `http://adresse_ip_externe:8889/?token=votre_token`
    
### Les librairies python à installer :

#### Installer tensorflow-gpu

(`pip3 uninstall protobuf` && `pip3 uninstall tensorflow` à faire au préalable si vous avez déjà tensorflow d'installé)

* `pip3 install tensorflow-gpu==2.2.0`
* `pip3 install keras`

## Installation de Nvidia et de cuda

### installez les drivers nvidia 

[https://gist.github.com/matheustguimaraes/43e0b65aa534db4df2918f835b9b361d](https://gist.github.com/matheustguimaraes/43e0b65aa534db4df2918f835b9b361d)

### Installation de CUDA 

Nous installerons CUDA 10.2, ce n'est pas la dernière version, mais c'est ce que supportent la plupart des librairies

[https://developer.nvidia.com/cuda-10.2-download-archive](https://developer.nvidia.com/cuda-10.2-download-archive)

* Sélectionner Linux / x86_64 / Ubuntu / votre version
* Choisissez la manière d'installer que vous préférez, par exemple **deb (local)** 

Vous pouvez ensuite suivre les commandes indiquées à l'écran.

### Tester le fonctionnement

Si tout fonctionne la commande  `nvidia-smi` doit afficher des informations sur votre gpu.

Avec Tensorflow: 

   * `physical_devices = tf.config.list_physical_devices('GPU')`
   * `print("Num GPUs:", len(physical_devices))`
   * (Faut faire gaffe, il y a des GPU qui s'affichent en XLA_GPU, qui ne seront donc pas listées avec cette commande)
   * `print(tf.config.list_physical_devices())`

### Troubleshoot : Aucune GPU listée :

* `pip3 show tensorflow-gpu` 
Vérifier que tensorflow-gpu==2.2.0 est installé sur l'environnement conda où vous lancez le jupyter.
Cette commande doit retourner la version 2.2 (voir au dessus, pour correspondre au cuda10.2 c'est nécessaire de pas mettre la dernière version dispo)

* `pip3 show tensorflow`
Cette commande ne devrait rien retourner, dans ce cas c'est OK


* Si lorsque vous listez les devices (avec la cellule d'au dessus) vous voyez dans la console : Could not load dynamic library 'libcudart.so.10.2' alors il faut hacker le code tensorflow pour qu'il la trouve (ou réinstaller depuis les sources, c'est ce qu'ils disent sur le tuto mais c'est embêtant)
    * `sudo ln -s /usr/local/cuda-10.2/targets/x86_64-linux/lib/libcudart.so.10.2 /usr/lib/x86_64-linux-gnu/libcudart.so.10.2`

* S'il demande une autre version de la librairie libcudart alors c'est qu'il faut que vous changiez la version de tensorflow-gpu pour qu'il change ses prérequis


* Si toujours pas bon, ajoutez ça à la fin du bashrc :
    * `export PATH=/usr/local/cuda-10.2/bin${PATH:+:${PATH}}`
    * `export LD_LIBRARY_PATH=/usr/local/cuda-10.2/lib64${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}`

Ca fonctionnera quand la cellule vous printera une ligne pour le CPU et une ou plusieurs lignes pour la GPU

### Check les versions CUDA, lib etc

* CUDA : `nvcc --version` (bien vérifier que le bin de cuda est atteignable (cf export PATH ci-dessus))
* Vérifier que la gpu est utilisable, check la puissance/mémoire utilisée : `nvidia-smi`
* tensorflow : `pip3 show tensorflow-gpu` ou dans le notebook `print tensorflow._version_`
* lister tous les packets python : `pip freeze`

## Uploadez des fichiers sur sa machine locale 

### Par ssh

* Ajout de la clé publique SUR VOTRE MACHINE PERSONNELLE : 
    * copier l'intégralité du fichier `~/.ssh/id_rsa.pub` (le créer avec la commande `ssh-keygen` s'il n'existe pas)
    * le coller SUR LA MACHINE GCP dans le fichier [TODO]

* Ensuite, pour copier MON_FICHIER de la machine locale vers la VM `scp /chemin/vers/MON_FICHIER  login@address_ip_externe:/chemin/sur/la/VM`

## TroubleShooting

### Problème de quotas 

Passez la limite de "Compute Engine API GPUs (all regions)" à 1:

* IAM et admin => quotas
* Cliquez le champ Filter table -> limit name -> faites déroulez la liste jusqu'à trouver GPU all regions.
* Sélectionnez-le, un bandeau doit apparaître à droite de l'écran. Cliquez "global" éditez les quotas  
* faire une demande pour le passer à 1. (parfois 15 min d'attente avant confirmation)